export type Role = {
  id: string
  name: string
  delete: Function
}

export type Client = {
  on: Function
}

export type Message = {
  author: {
    username: string
    id: string
  }
  content: string
  channel: {
    send: Function
    overWritePermissions: Function
    get: Function
  }
  reply: Function
  sendMessage: Function
  delete: Function
  guild: {
    createRole: Function
    ownerID: string
    channels: {
      get: Function
    }
    roles: {
      array: Function
      has: Function
      delete: Function
    }
  }
}
