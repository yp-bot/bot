/* global process */
import Discord from 'discord.js'
import logger from 'consola'

const client = new Discord.Client()

client.on('ready', () => logger.success('Ready!'))

client.on('error', err => logger.error(err))

export const init = () => client.login(process.env.BOT_TOKEN)

export { client }
