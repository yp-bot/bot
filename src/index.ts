import logger from 'consola'
import { init } from './client/'
import startListeners from './listeners/'

import db from '~/db/'

db
  .defaults({
    'half-voice': {}
  })
  .write()

init()
  .then(startListeners)
  .catch((err: string) => logger.error(err))
