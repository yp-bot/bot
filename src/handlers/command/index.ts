import fs from 'fs-extra'
import path from 'path'

import { kebabCase } from 'lodash'

import { Message } from '~/types/'

const commandPrefix = process.env.COMMAND_PREFIX || '!'
const commandExp = new RegExp(`${commandPrefix}[a-z\\-]+`, 'g')
const commandPath = path.resolve(__dirname, '..', '..', 'commands')

const parseCommand = (messageContent: string) => {
  // Split up the message by spaces first
  const [cmd, ...args] = messageContent.split(' ')
  // Parse out command and its prefix
  const command = cmd
    .trim()
    .toLowerCase()
    .match(commandExp)[0]
    .split(commandPrefix)[1]

  return { command, args }
}

/**
 * @description Gets a list of hardcoded commands
 */

const getHardcodedCommands = () =>
  fs
    .readdirSync(commandPath)
    .map(commandFile =>
      kebabCase(
        commandFile.split(
          process.env.NODE_ENV === 'development' ? '.ts' : '.js'
        )[0]
      )
    )

const hardcodedCommands = getHardcodedCommands()

const commands: Array<string> = [...hardcodedCommands]

// Get parsed command name and the message object
export default (message: Message) => {
  const { command, args } = parseCommand(message.content)

  // Loop through command names and then invoke their logic
  commands.forEach(
    commandName =>
      command === commandName &&
      import(path.resolve(commandPath, command)).then(commandFile =>
        commandFile.default(message, args)
      )
  )
}
