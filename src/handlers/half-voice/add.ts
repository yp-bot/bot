import logger from 'consola'
import { client } from '~/client/'
import { Role, Message } from '~/types/'

import db, { channelIsWatched } from '~/db/'

import { Args } from './'
import { configurePermissions, permissionDefaults } from './permissions'

import {
  modifyTextChannel,
  getRoleByName,
  fetchIdByChannelName
} from '~/helpers/channels'

/**
 * @description Create a half voice role
 * @param voiceChannel Name of a voice channel
 * @param message Message Object
 */
const createVoiceRole = async (
  voiceChannel: string,
  message: Message
): Promise<Role> => {
  const roleName = `half-voicers-${voiceChannel}`
  const role = await message.guild.createRole({
    name: `half-voicers-${voiceChannel}`,
    permissions: 37080128,
    mentionable: false
  })

  return role
}

/**
 * @param textChannels Array of voice channel names
 */
const getTextChannels = (textChannels: Array<string>): Array<string> =>
  textChannels.map(name => fetchIdByChannelName(name, 'text'))

/**
 * @description Entry point.
 */
export default async ({
  voiceChannelName,
  textChannels,
  message
}: Args): Promise<void> => {
  /* Get the ids */
  const voiceChannelId: string = fetchIdByChannelName(voiceChannelName, 'voice')
  const watchedChannels: Array<string> = getTextChannels(textChannels)

  if (channelIsWatched(voiceChannelId)) {
    return message.reply(
      `Sorry this voice channel is already watched... If you'd like to edit it you'll have to remove it first then readd`
    )
  }

  db.set(`half-voice.${voiceChannelId}`, watchedChannels).write()

  if (message.guild.roles.has(`half-voices-${voiceChannelName}`)) {
    return message.reply(`Okay! Watching channel ${voiceChannelName}`)
  }

  const [halfVoicersRole, allUserRole] = await Promise.all([
    createVoiceRole(voiceChannelName, message),
    getRoleByName('@everyone', message)
  ])

  /* Modify @everyone with new role methods */
  try {
    const [allUserPermissions, halfVoicersRolePermissions] = await Promise.all([
      configurePermissions(permissionDefaults, false),
      configurePermissions(permissionDefaults, true)
    ])

    await Promise.all([
      modifyTextChannel(watchedChannels, allUserRole, allUserPermissions),
      modifyTextChannel(
        watchedChannels,
        halfVoicersRole,
        halfVoicersRolePermissions
      )
    ])
  } catch (e) {
    logger.error(e)

    return message.reply(`Something's fucky.  Try again later`)
  }

  return message.reply(
    `${voiceChannelName} is watched! Permissions are setup, and I created a role called ${
      halfVoicersRole.name
    } :tada:`
  )
}
