interface PermissionIndex {
  [permissionName: string]: Boolean
}

export const permissionDefaults: Array<string> = [
  'SEND_MESSAGES',
  'SEND_TTS_MESSAGES',
  'EMBED_LINKS',
  'ATTACH_FILES',
  'MENTION_EVERYONE',
  'ADD_REACTIONS'
]

export const configurePermissions = async (
  permissionNames: Array<string>,
  permissionKey: Boolean
): Promise<Object> => {
  let permissions: PermissionIndex = {}

  permissionNames.forEach(permissionName => {
    return (permissions[permissionName] = permissionKey)
  })

  return permissions
}
