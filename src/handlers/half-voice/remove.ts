import logger from 'consola'
import { client } from '~/client/'
import db from '~/db/'
import { Message, Role } from '~/types/'

import { to } from '~/helpers/'
import {
  modifyTextChannel,
  getRoleByName,
  fetchIdByChannelName
} from '~/helpers/channels'

import { Args } from './'
import { configurePermissions, permissionDefaults } from './permissions'

interface PermissionIndex {
  [permissionName: string]: Boolean
}

const removeVoiceRole = async (voiceChannelName: string, message: Message) => {
  const roleName: string = `half-voicers-${voiceChannelName}`
  const voiceRole: Role = await to(getRoleByName, e => logger.error(e))(
    roleName,
    message
  )

  try {
    return voiceRole.delete()
  } catch (e) {
    logger.error(e)
  }
}

export default async ({ voiceChannelName, textChannels, message }: Args) => {
  const voiceChannelId = fetchIdByChannelName(voiceChannelName, 'voice')
  const watchedChannels: Array<string> = db
    .get(`half-voice.${voiceChannelId}`)
    .value()

  const voiceChannel = message.guild.channels.get(voiceChannelId)

  // const [permissions, ...stuff] = await to(Promise.all, e => new Error(e))([
  //   configurePermissions(permissionDefaults, null),
  //   removeVoiceRole(voiceChannelName, message)
  // ])
  const [permissions, ...rest] = await Promise.all([
    configurePermissions(permissionDefaults, null),
    removeVoiceRole(voiceChannelName, message)
  ])

  const allUsersRole: Role = await to(getRoleByName, e => new Error(e))(
    '@everyone',
    message
  )

  await to(modifyTextChannel, e => new Error(e))(
    watchedChannels,
    allUsersRole,
    permissions
  )

  db.unset(`half-voice.${voiceChannelId}`).write()

  message.reply(`Remove watched channel: ${voiceChannelName}`)
}
