import { Message } from '~/types/'

import add from './add'
import remove from './remove'

export type Args = {
  voiceChannelName: string
  textChannels: Array<string>
  message: Message
}

interface IndexType {
  [key: string]: Function
}

const methods: IndexType = {
  add,
  remove
}
export default methods
