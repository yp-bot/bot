import logger from 'consola'

import db from '~/db/'

import { Role } from '~/types/'

import { getRoleByName } from '~/helpers/channels'

type Collection = {
  id: string
  name: string
}

const dbHasID = (id: string) =>
  db
    .get('half-voice')
    .has(id)
    .value()

export const connected = async (member: any) => {
  const voiceChannelID: string = member.voiceChannelID
  const voiceChannel: Collection = member.guild.channels.get(voiceChannelID)

  // logger.info(`User connected to ${voiceChannel.name}`)

  if (!dbHasID(voiceChannel.id)) {
    return
  }

  const voiceRole = await getRoleByName(
    `half-voicers-${voiceChannel.name}`,
    member
  )

  logger.info(`${member.user.username} connected to ${voiceChannel.name}`)
  return member.addRole(
    voiceRole.id,
    `User ${member.user.username} connected to voice channel`
  )
}

export const disconnected = async (member: any) => {
  const voiceChannelID: string = member.voiceChannelID
  const voiceChannel: Collection = member.guild.channels.get(voiceChannelID)

  if (!dbHasID(voiceChannel.id)) {
    return
  }

  const voiceRole = await getRoleByName(
    `half-voicers-${voiceChannel.name}`,
    member
  )
  logger.info(
    `${member.user.username} disconnected from voice channel: ${
      voiceChannel.name
    }`
  )
  return member.removeRole(
    voiceRole.id,
    `User ${member.user.username} disconnected from voice channel`
  )
}

export const switched = async (newMember: any, oldMember: any) => {
  if (newMember.voiceChannelID === oldMember.voiceChannelID) {
    return
  }

  logger.info(`${newMember.user.username} Switched voice channels`)

  await disconnected(oldMember)

  return connected(newMember)
}
