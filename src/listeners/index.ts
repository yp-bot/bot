// const fs = require('fs-extra')
import fs from 'fs-extra'
import path from 'path'

import { client } from '~/client/'

/**
 * @description Dynamically init listeners
 * @param {Array} dirs An array of local folders
 */
const readForDirs = (dirs: Array<string>) =>
  dirs.forEach(async (dir: string) => {
    const dirPath = path.resolve(__dirname, dir)
    const stats = await fs.lstat(dirPath)

    // Check the fs stat if it is a directory
    if (stats.isDirectory()) {
      import(dirPath).then(listener => listener.default(client))
    }
  })

// Init the listeners here
export default () =>
  fs
    .readdir(path.join(__dirname))
    .then(readForDirs)
    .catch(err => new Error(err))
