const commandPrefix = process.env.COMMAND_PREFIX || '!'

import { Message } from '~/types/'

import messageHandler from '~/helpers/message'

import handleCommand from '~/handlers/command/'

const checkMessageForCommand = (message: Message) => {
  if (!message.content.startsWith(commandPrefix)) {
    return
  }
  handleCommand(message)
}

const checkForYouPeople = (message: Message) =>
  message.content
    .trim()
    .toLowerCase()
    .includes('you people')
    ? messageHandler.reply(message, 'Fuck you mean you people?')
    : {}

export default (client: any) =>
  client.on('message', (message: any) => {
    checkForYouPeople(message)
    checkMessageForCommand(message)
  })
