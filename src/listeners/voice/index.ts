import { client } from '~/client/'

import { Client } from '~/types/'

import * as handlers from '~/handlers/connection/'

const checkConnectedStates = (oldChannelID: string, newChannelID: string) => {
  /* User connected */
  if (oldChannelID && newChannelID) {
    return 'switched'
  }
  if (!oldChannelID && newChannelID) {
    return 'connected'
  } else if (oldChannelID && !newChannelID) {
    return 'disconnected'
  }

  return null
}

export default (client: Client) =>
  client.on('voiceStateUpdate', (oldMember: any, newMember: any) => {
    const userStates: string = checkConnectedStates(
      oldMember.voiceChannelID,
      newMember.voiceChannelID
    )

    /* Something happened but just leave it for now */
    if (!userStates) {
      return
    }

    switch (userStates) {
      case 'connected':
        return handlers.connected(newMember)
      case 'disconnected':
        return handlers.disconnected(oldMember)
      case 'switched':
        return handlers.switched(newMember, oldMember)
    }
  })
