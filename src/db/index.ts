import low from 'lowdb'
import FileSync from 'lowdb/adapters/FileSync'

const adapter = new FileSync('db.json')

const db = low(adapter)

export const channelIsWatched = (id: string) =>
  db
    .get('half-voice')
    .has(id)
    .value()

export default db
