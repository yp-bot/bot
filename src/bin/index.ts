import logger from 'consola'
import resolver from 'module-alias'
import path from 'path'

resolver.addAliases({
  '~': path.join(__dirname, '..')
})

resolver()

logger.start('Starting bot...')
