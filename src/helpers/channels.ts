import { client } from '~/client/'
import { Message, Role } from '~/types/'

/**
 * @description Gets an id from a channel name
 * @param channelName name of the discord channel
 * @param type The type of discord channel.  Either voice or text
 */
export const fetchIdByChannelName = (
  channelName: string,
  type: string
): string => {
  const channel = client.channels.filter(
    (channel: any) => channel.type === type && channel.name === channelName
  )

  if (channel.array().length === 1) {
    return channel.firstKey()
  }
}

/**
 * @description Overwrite permissions for an array of channel ids
 */
export const modifyTextChannel = async (
  channelIds: Array<string>,
  user: Role,
  permissions: Object
): Promise<void> => {
  return channelIds.forEach((id: string) => {
    const channel: any = client.channels.get(id)

    return channel.overwritePermissions(
      user,
      permissions,
      `Setting voice chat permissions for channel: ${id}`
    )
  })
}

/**
 * @description Get the role by name
 * @param roleName Name of role
 * @param message Message object
 */
export const getRoleByName = async (
  roleName: string,
  message: Message
): Promise<Role> => {
  const role = message.guild.roles
    .array()
    .find((role: Role) => role.name === roleName)

  return role
}
