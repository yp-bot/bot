import { noop } from 'lodash'
import message from './message'

export const to = (func: Function, errorFunc = noop) => (
  ...params: Array<any>
) => func(...params).catch(errorFunc)

// module.message = message
export { message }
