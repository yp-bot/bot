import { client } from '~/client/'

import { Message } from '~/types/'

const authorIsBot = (message: Message) => message.author.id === client.user.id

export default {
  reply: (message: Message, requestedResponse: string) =>
    !authorIsBot(message) ? message.reply(requestedResponse) : {},

  sendPrivateMessage: (message: Message, requestedResponse: string) =>
    message.sendMessage(requestedResponse),

  deleteMessage: (message: Message) => message.delete()
}
