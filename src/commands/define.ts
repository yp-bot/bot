import fetch from 'node-fetch'

import { kebabCase, upperFirst, isEmpty } from 'lodash'

import { Message } from '~/types/'

const urbanDictionaryApi: string =
  'http://api.urbandictionary.com/v0/define?term='

type DefObject = {
  definition: string
  example: string
}

const getRandomDef = (defArr: Array<DefObject>): DefObject => {
  const arrItemNum: number = defArr.length
  const randomNum: number = Math.floor(Math.random() * arrItemNum)
  const definition = defArr[randomNum]
  return definition
}

export default async (message: Message, args: Array<string>) => {
  const term: string = kebabCase(args.join(' '))

  const response = await fetch(urbanDictionaryApi + term)
  const { list } = await response.json()

  if (isEmpty(list)) {
    return message.reply(`I couldn't find a definition for _${args.join(' ')}_`)
  }

  const { definition, example } = getRandomDef(list)

  message.reply()
  message.channel.send({
    content: `@${message.author.username}`,
    embed: {
      color: 15728384,
      description: definition,
      title: upperFirst(term),
      fields: [
        {
          name: 'Example',
          value: example
        }
      ]
    }
  })
}
