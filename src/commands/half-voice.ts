import { client } from '~/client/'
import { Message, Role } from '~/types/'

import actions from '~/handlers/half-voice/'
/*
  The add command should take the choice channel and the text channels that are linked to it.
  Command Example:
  !half-voice add <voice-channel> ...text-channels

  The remove command should remove a voice channel from the db list and all text channels
  !half-voice remove <voice-channel-name>

  Can take either a channel name or id
*/

type Actions = {
  add: Function
  remove: Function
}

export default async (message: Message, args: Array<string>) => {
  if (message.guild.ownerID !== message.author.id) {
    return message.reply(
      `You can't use this command because you're not the owner of the server`
    )
  }
  // Get the first flag
  const [actionName, ...rest] = args

  const [voiceChannelName, ...textChannels] = rest

  if (!voiceChannelName) {
    return message.reply('Missing a voice channel')
  }

  if (textChannels.length === 0 && actionName === 'add') {
    return message.reply('You have no text channels to watch')
  }

  return actions[actionName]({ voiceChannelName, textChannels, message })
}
